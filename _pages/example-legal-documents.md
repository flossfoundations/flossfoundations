---
layout: single
title: "Example Legal Documents"
permalink: /node/13
---

Links to various FLOSS-related contributor agreements and trademark policies.

* Contributor Licensing Agreements
    * [Apache Individual Contributor Licensing Agreement](http://www.apache.org/licenses/icla.txt)
    * [Apache Corporate Contributor Licensing Agreement](http://www.apache.org/licenses/cla-corporate.txt)
    * [Sun Joint Contributor Agreement](http://www.sun.com/software/opensource/contributor_agreement.jsp)
* Trademark Policies
    * Apache
    * MySQL Trademark Policies

If you are a Free Software or Open Source project who needs help adapting one of these licenses to meet your needs. Write info@softwarefreedom.org
