---
layout: single
title: "FLOSS Foundations Software for Nonprofits"
permalink: /node/15
---

## Donation Management

* LedgerSMB -- Not Ready Yet.
    * Josh creating software from scratch.
    * Discussed funding, etc.
* OpenERP / TinyERP
    * Need to check out -- can we adapt it?
* DonorWare proprietary. Probably won't open source.

## Event Management

* PentaBarf Content Management
* Linux Fund RSVP system -- BETA
* Eventnations -- O'Reilly System, may be open sourced.
* People using Meetup.com -- expensive

## CRM

* SugarCRM, vTiger
* Concursive
* CiviCRM
* Aldil French membership management software

## Payroll

Why not just use payroll services? Too difficult to maintain rules/taxes yourself

* Paychex

## Working Group

TBA. Josh Berkus, Bradley, Others.
